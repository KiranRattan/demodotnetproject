﻿using AppCardioModels.Models;
using AppCardioModels.ViewModels;
using System.Collections.Generic;

namespace AppCardioRepository.Interface
{
    public interface IZephyrRepository
    {
        bool InsertZephyrParameters(TblZephirData objtblData);
        List<ZephyrModel> GetZephyrParameters(ZephyrFiltersModel objZephyrFiltersModel);
        ZephyrECGDataViewmodel GetZephyrECGMQTTData(string patientId);

    }
}
