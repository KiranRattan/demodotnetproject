﻿using AppCardioModels.ViewModels;
using AppCardioRepository.Context;
using AppCardioRepository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using AppCardioCommon;
using Microsoft.EntityFrameworkCore;
using AppCardioModels.Models;

namespace AppCardioRepository.Repository
{
    public class ZephyrRepository : IZephyrRepository
    {
        private readonly AppCardioDBContext _appCardioDBContext;
        public ZephyrRepository(AppCardioDBContext appCardioDBContext)
        {
            _appCardioDBContext = appCardioDBContext;

        }

        /// <summary>
        /// InsertZephyrParameters
        /// </summary>
        /// <param name="objtblData"></param>
        /// <returns></returns>
        public virtual bool InsertZephyrParameters(TblZephirData objtblData)
        {
            try
            {
                _appCardioDBContext.Add(objtblData);
                _appCardioDBContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// GetZephyrParameters
        /// </summary>
        /// <param name="objZephyrFiltersModel"></param>
        /// <returns></returns>
        public virtual List<ZephyrModel> GetZephyrParameters(ZephyrFiltersModel objZephyrFiltersModel)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[7];
                param[0] = new SqlParameter("@zephyrDataId", objZephyrFiltersModel.Id);
                param[1] = new SqlParameter("@deviceMacAddress", objZephyrFiltersModel.deviceMacAddress);
                if (objZephyrFiltersModel.createdDate.HasValue)
                    param[2] = new SqlParameter("@date", objZephyrFiltersModel.createdDate.Value);
                else
                    param[2] = new SqlParameter("@date", DBNull.Value);
                param[3] = new SqlParameter("@SortType", objZephyrFiltersModel.SortType);
                param[4] = new SqlParameter("@SortCol", objZephyrFiltersModel.SortCol);
                param[5] = new SqlParameter("@Skip", objZephyrFiltersModel.Skip);
                param[6] = new SqlParameter("@PageSize", objZephyrFiltersModel.PageSize);

                // var zephirDataList = _appCardioDBContext.TblZephirData.FromSql(DBConstant.usp_GetZephyrDataList, param).ToList();
                var zephirDataList = _appCardioDBContext.Query<ZephyrModel>().FromSql(DBConstant.usp_GetZephyrDataList, param).ToList();
                return zephirDataList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        /// <summary>
        /// Get Zephyr ECG Data Using By PatientId
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        public virtual ZephyrECGDataViewmodel GetZephyrECGMQTTData(string patientId)
        {
            try
            {
                var dataECG = _appCardioDBContext.TblZephyrEcgdata.Where(x => x.PatientId == "2");
                var dataRTR = _appCardioDBContext.TblZephyrRtrdata.Where(x => x.PatientId == "2");
                if (dataECG.Count() > 0 || dataRTR.Count() >0)
                {
                    ZephyrECGDataViewmodel objModel = new ZephyrECGDataViewmodel();

                    objModel.PatientId = patientId;
                    List<ECGDataModel> listECGDataModel = new List<ECGDataModel>();
                    List<RTRDataModel> listRTRDataModel = new List<RTRDataModel>();
                    foreach (var tblData in dataECG)
                    {
                        ECGDataModel model = new ECGDataModel();
                        //model.ecgValue = tblData.Ecgdata;
                        //model.ecgTime = tblData.EcgdateTime.ToString();

                        listECGDataModel.Add(model);
                    }
                    objModel.ECGDetails = listECGDataModel;

                    foreach (var tblData in dataRTR)
                    {
                        RTRDataModel model = new RTRDataModel();
                        model.RTRValue = tblData.Rtrdata;
                        model.RTRTime = tblData.RtrdateTime.ToString();

                        listRTRDataModel.Add(model);
                    }
                    objModel.RTRDetails = listRTRDataModel;
                    return objModel;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
