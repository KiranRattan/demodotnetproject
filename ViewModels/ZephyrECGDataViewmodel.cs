﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppCardioModels.ViewModels
{
    public class ZephyrECGDataViewmodel
    {
        public string PatientId { get; set; }
        public List<ECGDataModel> ECGDetails { get; set; }
        public List<RTRDataModel> RTRDetails { get; set; }
    }
}
