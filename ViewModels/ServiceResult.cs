﻿
namespace AppCardioModels.ViewModels
{
    public class ServiceResult
    {
        public string Message { get; set; }
        public bool Status { get; set; }
        public int StatusCode { get; set; }
        public dynamic ResultData { get; set; }
        public string Resourcetype { get; set; }
        public MetaData metaData;
    }
    public class MetaData
    {
        public int pageNo { get; set; }
        public int skip { get; set;}
        public int RecordCount { get; set; }
    }
}
