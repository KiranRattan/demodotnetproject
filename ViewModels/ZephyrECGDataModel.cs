﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppCardioModels.ViewModels
{
    public class ZephyrECGDataModel
    {
        public int Id { get; set; }
        public string PatientId { get; set; }
        public byte[] ECGDetails { get; set; }
        public List<ECGDataModel> listECGDataModel { get; set; }
        public List<RTRDataModel> listRTRDataModel { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? Createdate { get; set; }
    }
    public class ECGDataModel
    {
        public string ecgValue { get; set; }
        public string ecgTime { get; set; }
    }
    public class RTRDataModel
    {
        public string RTRValue { get; set; }
        public string RTRTime { get; set; }
    }
}
