﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppCardioModels.ViewModels
{
    public class ZephyrDataViewModelList
    { 
        public List<ZephyrDataViewModel> data { get; set; }
    }
    public class ZephyrDataViewModel
    {
        //public int Id { get; set; }
        public string PostureAngle { get; set; }
        public string SkinTemperature { get; set; }
        public string HeartRate { get; set; }
        public string BreathingRate { get; set; }
        public string BatteryLevel { get; set; }
        public string PatientId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string RTR { get; set; }
        public string DeviceMacAddress { get; set; }

         
        public string CreatedBy { get; set; }
    }
}
