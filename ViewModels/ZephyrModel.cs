﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AppCardioModels.ViewModels
{
    public class ZephyrModel
    {
        public int Id { get; set; }
        [Required]
        public string PostureAngle { get; set; }
        [Required]
        public string SkinTemperature { get; set; }
        [Required]
        public string HeartRate { get; set; }
        [Required]
        public string BreathingRate { get; set; }
        [Required]
        public string BatteryLevel { get; set; }
        [Required]
        public string PatientId { get; set; }
        [Required]
        public string DeviceMacAddress { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
       

        public int RecordCount { get; set; }
    }

    public class ZephyrFiltersModel
    {
        public int Id { get; set; }  
        public string deviceMacAddress { get; set; }
        public DateTime? createdDate { get; set; }
        public string SortType { get; set; }
        public string SortCol { get; set; }
        public int Skip { get; set; }
        public int PageSize { get; set; }
    }


}
