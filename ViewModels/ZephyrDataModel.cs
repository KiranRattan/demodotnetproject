﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppCardioModels.Models
{
    public class ZephyrDataModel
    {
        public string PatientId { get; set; }
        public int TimeType { get; set; }
        public string Date { get; set; }
        public string CountryID { get; set; }
    }
}
