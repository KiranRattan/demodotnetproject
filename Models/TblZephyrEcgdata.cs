﻿using System;
using System.Collections.Generic;

namespace AppCardioModels.Models
{
    public partial class TblZephyrEcgdata
    {
        public int Id { get; set; }
        public string PatientId { get; set; }
        public string ECGData { get; set; }
        public DateTime? ECGDateTime { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
