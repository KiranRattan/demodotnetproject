﻿using System;
using System.Collections.Generic;

namespace AppCardioModels.Models
{
    public partial class TblZephirData
    {
        public int Id { get; set; }
        public string PostureAngle { get; set; }
        public string SkinTemperature { get; set; }
        public string HeartRate { get; set; }
        public string BreathingRate { get; set; }
        public string BatteryLevel { get; set; }
        public string PatientId { get; set; }
        public string DeviceMacAddress { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Rtr { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
