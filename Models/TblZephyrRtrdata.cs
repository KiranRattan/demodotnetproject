﻿using System;
using System.Collections.Generic;

namespace AppCardioModels.Models
{
    public partial class TblZephyrRtrdata
    {
        public int Id { get; set; }
        public string PatientId { get; set; }
        public string Rtrdata { get; set; }
        public DateTime? RtrdateTime { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
