﻿using System;
using Microsoft.AspNetCore.Mvc;
using AppCardioBLL.Interface;
using AppCardioModels.ViewModels;
using System.Collections.Generic;

namespace AppCardioAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ZephyrController : ControllerBase
    {
        private readonly IZephyrService _zephyrService;
        public ZephyrController(IZephyrService zephyrService)
        {
            _zephyrService = zephyrService;
        }

        /// <summary>
        /// InsertZephyrParameters
        /// </summary>
        /// <param name="objZephyrModel"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult InsertZephyrParameters(ZephyrModel objZephyrModel)
        {
            try
            {
                if (ModelState.IsValid && objZephyrModel != null)
                {
                    var result = _zephyrService.InsertZephyrParameters(objZephyrModel);
                    return Ok(result);
                }
                else
                {
                    return BadRequest();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetZephyrParameters
        /// </summary>
        /// <param name="objZephyrFiltersModel"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult GetZephyrParameters(ZephyrFiltersModel objZephyrFiltersModel)
        {
            try
            {
                if (ModelState.IsValid && objZephyrFiltersModel != null)
                {
                    var result = _zephyrService.GetZephyrParameters(objZephyrFiltersModel);
                    return Ok(result);
                }
                else
                {
                    return BadRequest();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Zephyr ECG Data Using By PatientId
        /// </summary>
        /// <param name="patientId"></param>
        /// <returns></returns>
        ///
        [HttpGet]
        public IActionResult GetZephyrECGMQTTData(string patientId)
        {
            try
            {
                if (patientId != null)
                {
                    var result = _zephyrService.GetZephyrECGMQTTData(patientId);
                    return Ok(result);
                }
                else
                {
                    return BadRequest();
                } 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}